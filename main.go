package main

import (
	"fmt"
	"os"
)

func main() {

	argsWithoutProg := os.Args[1:]

	id := argsWithoutProg[0]

	message := []byte(argsWithoutProg[1])
	fmt.Println("Id: " + id + " Message: " + string(message))

	aesKey, _ := store(id, message)
	payload, _ := retrieve(id, aesKey)

	fmt.Println("Decrypted message = " + string(payload))
}
