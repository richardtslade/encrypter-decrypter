package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"

	"golang.org/x/crypto/argon2"
)

var storeMap = make(map[string][]byte)

func randStr(strSize int, randType string) []byte {

	var dictionary string

	if randType == "alphanum" {
		dictionary = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	}

	if randType == "alpha" {
		dictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	}

	if randType == "number" {
		dictionary = "0123456789"
	}

	var bytes = make([]byte, strSize)
	rand.Read(bytes)
	for k, v := range bytes {
		bytes[k] = dictionary[v%byte(len(dictionary))]
	}
	return bytes
}

func store(id string, payload []byte) (aesKey []byte, err error) {
	salt := randStr(32, "alphanum")

	argon2Key := argon2.IDKey([]byte(id), salt, 1, 64*1024, 4, 32)

	block, err := aes.NewCipher(argon2Key)

	blockStr := base64.StdEncoding.EncodeToString(payload)

	ciphertext := make([]byte, aes.BlockSize+len(blockStr))

	iv := ciphertext[:aes.BlockSize]

	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(blockStr))

	storeMap[string(argon2Key)] = ciphertext

	return argon2Key, err
}

func retrieve(id string, aesKey []byte) (payload []byte, err error) {
	ciphertext := storeMap[string(aesKey)]

	block, err := aes.NewCipher(aesKey)

	iv := ciphertext[:aes.BlockSize]

	ciphertext = ciphertext[aes.BlockSize:]

	decrypter := cipher.NewCFBDecrypter(block, iv)
	decrypter.XORKeyStream(ciphertext, ciphertext)

	data, err := base64.StdEncoding.DecodeString(string(ciphertext))

	return data, err
}
